https://itnext.io/journey-of-a-microservice-application-in-the-kubernetes-world-3c2a9e701e9f

multipass launch --name kube --cpus 4 --mem 4G --disk 10G

multipass info kube
multipass list
multipass delete kube
multipass purge

multipass shell kube
curl -sfL https://get.k3s.io | K3S_KUBECONFIG_MODE="644" INSTALL_K3S_EXEC="server --disable=traefik" sh -

multipass exec kube -- sudo cat /etc/rancher/k3s/k3s.yaml > $HOME/config

kubectl config set-context --current --namespace webhooks

k delete ns webhooks && k create ns webhooks 


kompose convert -f docker-compose.yaml

helm create webhooks

helm upgrade --install webhooks . -n webhooks -f values.yaml

helmfile destroy
helmfile apply

kubectl port-forward  wss-75b5484cbf-j5h8x 8080:8080

helm plugin install https://github.com/databus23/helm-diff
helmfile apply traefik.yaml

## Production

k create ns webhooks
kubectl config set-context --current --namespace webhooks
k apply -f domain/smter-ml.yaml
k apply -f helmfiles/redirect-https.yaml
k apply -f helmfiles/protomidleware.yaml
k apply -f helmfiles/cors-midleware.yaml


kustomize build manifests/base > deploy.yaml
k apply -k  manifests/base

k apply -f deploy.yaml
k apply -f helmfiles/ingressroute.yaml
k apply -f helmfiles/middleware.yaml

kubectl port-forward  wss-75b5484cbf-j5h8x 8080:8080


wscat -c "ws://10.137.49.167/ws?token=84432a036094ad2a1e64a4c6d6fd99" -n -H Authorization:"Bearer token-qt999:kcw55kqgkwsgsj55q2ps6gmpvw99c5vfp5mt4sh8kkwz5mnmk55x2s" -H x-forwarded-proto:"https"

websocat  ws://localhost/ws?token=a7e818c658b81035544bdd0417b378
websocat ws://10.137.49.167:8080/ws?token=c9aa5c8fc7e0b62a24b90767aba6e8
websocat ws://10.137.49.167/ws?token=f052ddfa39dfe62f792081eecbc64a

docker run -p 127.0.0.1:8080:8080/tcp  remotejob/swaggerui:latest

export GITLAB_TOKEN=
helm repo add grafana https://grafana.github.io/helm-charts

helm install my-loki grafana/loki --version 3.0.7 --set grafana.enabled=true --set prometheus.enabled=true

kubectl get secret loki-grafana --namespace=loki -o jsonpath="{.data.admin-password}" | base64 --decode ; echo

echo "adminadmin" | base64 YWRtaW5hZG1pbgo=

kubectl edit secrets loki-grafana -n loki

kubectl -n loki rollout restart deployment loki-prometheus-server
kubectl -n loki rollout restart statefulset loki

kubectl port-forward --namespace loki service/loki-grafana 3000:80

kubectl get pods --namespace loki -l "app=grafana,release=grafana" -o jsonpath="{.items[0].metadata.name}"

deploys=`kubectl -n loki get deployments | tail -n +2 | cut -d ' ' -f 1`
for deploy in $deploys; do
  kubectl -n loki rollout restart deployments/$deploy
done

plugins:
  - "@semantic-release/commit-analyzer"
  - "@semantic-release/release-notes-generator"
  - - "@semantic-release/changelog"
    - changelogFile: CHANGELOG.md
  - "@semantic-release/gitlab"
branches:
  - "main"

k config get-contexts 
k config use-context prod



