## Purpose

Simple application providing always-on webhooks for tests and demo purposes

## Architecture

The application follows a <strong>MICROSERVICE</strong> architecture, it is composed of: 

- www: the frontend web
- api: microservice which manipulates the data and communicate with an underlying database
- ws: <strong>websocket</strong> server to make sure the frontend is updated in real time each time the application receives a new payload

On top of those micro-services, additional components are used: 

- NATS message broker
- MongoDB database
- Traefik reverse proxy in front of the application  

## CI/CD and GitOps

```
Based on GitLab CI ----> Flux
Flux can be easily replaced by ArgoCD

```

The schema below illustrates the architecture of the application: 

![Architecture](./images/architecture.png)


The application will be available on https://smter.ml

## Usage

First go on the landing page

![Landing page](./images/wh-1.png)

Then scroll down to get the "Get my webhook" button

![Get your webhook 1](./images/wh-2.png)

Click on the button and get the information for you dedicated webhook

![Get your webhook 2](./images/wh-3.png)

Go to the dashboard (will be empty as no data have been sent to your webhook yet)

![Dashboard](./images/wh-4.png)

From a terminal, curl the simple curl command to send a first payload to your webhook (your token will be different than the one used in this example):

```
curl -XPOST -H "Authorization: Bearer 7e8d8c4d5df7a7a56345a7832c60de" -H 'Content-Type: application/json' -d '{"msg": "demo"}' localhost/data
```

See the json payload appearing in real time on your dashboard

![Data received](./images/wh-5.png)



